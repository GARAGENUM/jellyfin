# JELLYFIN

Véritable Media server/streamer open source via Docker
Si vous chercher une alternative à Plex ou Kodi gratuit, vous l'avez trouvé!

documentation: https://jellyfin.org/docs/

## CONFIGURATION

Modifier les "path" vers vos dossiers contenant les médias (films, séries, musique)

## USAGE

```bash
docker-compose up -d
```

Visiter localhost:8096 pour finir la configuration

Pour les clients:
* Android: https://play.google.com/store/apps/details?id=org.jellyfin.mobile
* Android TV: https://play.google.com/store/apps/details?id=org.jellyfin.androidtv
* IOS: https://apps.apple.com/fr/app/jellyfin-mobile/id1480192618
